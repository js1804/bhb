package domain;

import java.util.List;
import domain.exceptions.EnrollmentRulesViolationException;

public class EnrollCtrl {
	private Bool check_course_passed(Course pre, List<StudyRecord> transcript)
	{
		for (StudyRecord sr : transcript) {
			if (sr.getOffering().getCourse().equals(pre) && sr.getGrade() >= 10)
				return true;
		}
		return false;
	}
	private void check_pre_reqs(Student s, List<Offering> ol) throws EnrollmentRulesViolationException
	{
		for (Offering o : ol) 
		{
			List<Course> prereqs = o.getCourse().getPrerequisites();
			List<StudyRecord> transcript = s.getStudyRecords();
			for (Course pre : prereqs) {
				if(!check_course_passed(pre, transcript))
					throw new EnrollmentRulesViolationException(String.format("The student has not passed %s as a prerequisite of %s", pre.getName(), o.getCourse().getName()));
			}
		}
	}
	private void check_duplicate_passing(Student s, List<Offering> ol) throws EnrollmentRulesViolationException
	{
		List<StudyRecord> transcript = s.getStudyRecords();
		for (Offering o : ol) {
			for (StudyRecord sr : transcript) {
				if (check_course_passed(sr, transcript))
					throw new EnrollmentRulesViolationException(String.format("The student has already passed %s", o.getCourse().getName()));
			}
		}
	}


	private void check_overlap_exam(List<Offering> ol)throws EnrollmentRulesViolationException
	{
		for (Offering o1 : ol) {
			for (Offering o2 : ol) {
				if (o1 == o2)
					continue;
				if (o1.getExamTime().equals(o2.getExamTime()))
					throw new EnrollmentRulesViolationException(String.format("Two offerings %s and %s have the same exam time", o1, o2));
			}
		}
	}

	private void check_offer_is_unique(List<Offering> ol)throws EnrollmentRulesViolationException
	{
		for (Offering o1 : ol) {
			for (Offering o2 : ol) {
				if (o1 == o2)
					continue;
				if (o1.getCourse().equals(o2.getCourse()))
					throw new EnrollmentRulesViolationException(String.format("%s is requested to be taken twice", o1.getCourse().getName()));
			}
		}
	}
	private double calculate_gpa(Student s,List<Offering> ol)
	{
		double points = 0;
		int totalUnits = 0;
		for (StudyRecord sr : s.getStudyRecords()) {
			points += sr.getGrade() * sr.getUnits();
			totalUnits += sr.getUnits();
		}
		return points / totalUnits;
	}
	private int calc_unit_sum(,List<Offering> ol)
	{
		int unitsRequested = 0;
		for (Offering o : ol)
			unitsRequested += o.getUnits();
		return unitsRequested;
	}
	private void check_education_limits(Student s,List<Offering> ol) throws EnrollmentRulesViolationException
	{
		int unitsRequested = calc_unit_sum(ol);
		double gpa = calculate_gpa(s);
		if ((gpa < 12 && unitsRequested > 14) ||
				(gpa < 16 && unitsRequested > 16) ||
				(unitsRequested > 20))
			throw new EnrollmentRulesViolationException(String.format("Number of units (%d) requested does not match GPA of %f", unitsRequested, gpa));
	}
	public void enroll(Student s, List<Offering> ol) throws EnrollmentRulesViolationException {
		check_pre_reqs(s, ol);
		check_duplicate_passing(s, ol);
		check_overlap_exam(ol);
		check_offer_is_unique(ol);
		check_education_limits(s,ol);
		for (Offering o : ol)
			s.takeOffering(o);
	}
}
